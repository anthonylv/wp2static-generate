This directory can contain and files and directories that are not included by WP2Static but are needed for the live site.

Examples:
* Live .htaccess
* Live robots.txt
* Search engine authentication files
* External media directories
