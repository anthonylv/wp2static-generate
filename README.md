# WP2Static Generate

A general purpose static site deployment script using WP-CLI and WP2Static.
This script generates a static site from a local WordPress installation using the WP2Static plugin.

By Anthony Lopez-Vito of Another Cup of Coffee Limited
[https://anothercoffee.net](https://anothercoffee.net)

CAUTION: Make a backup of your database and files before running this tool. USE IS ENTIRELY AT YOUR OWN RISK.

All code is released under The MIT License.
Please see LICENSE.txt.

## Requirements

* WP-CLI
* WP2Static plugin

## Usage

`static_gen.sh [-d] URL`

Options
 `-d:`	Deployment URL

### Examples

Generate the static site using default deployment URL
	`static_gen.sh`

Generate the static site using default deployment supplied in options
	`static_gen.sh -d http://example.com`

## Resources

* [https://wp2static.com/developers/wp-cli/](https://wp2static.com/developers/wp-cli/)
* [https://developer.wordpress.org/cli/commands/](https://developer.wordpress.org/cli/commands/)


## License
Written by Anthony Lopez-Vito of [Another Cup of Coffee Limited](https://anothercoffee.net). All code is released under The MIT License.
Please see LICENSE.txt.
